const config = require('config')
const http = require('http')

if (!config.get("myprivatekey")) {
    console.error("FATAL ERROR: myprivatekey is not defined.");
    process.exit(1);
  }

const app = require('../lift/boot/server.js')

http.createServer(app).listen(config.get('server.port')), () => {
    console.log("Server was started on port " + config.get('server.port'))
}
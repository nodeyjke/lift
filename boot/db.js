const mongoose = require('mongoose');
const { Admin } = require('../src/entities/admin/model.js')
const config = require('config');
const bcrypt = require('bcryptjs')

module.exports = async () => {
  try {
    await mongoose.connect(config.get('db.mongoUrl'), {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    console.log("Connected to database")
    const admin = await Admin.findOne({
      name: config.get("admin.name"),
      email: config.get("admin.email")})
      const pass = await admin.comparePassword(config.get("admin.password"))
    if(admin && pass) {
      console.log(admin)
      }
      else{
        await Admin.create({
          name: config.get("admin.name"),
          email: config.get("admin.email"),
          password: await bcrypt.hash(config.get("admin.password"), 10)})
      }
  } catch (error) {
    console.log(error);
  }
};
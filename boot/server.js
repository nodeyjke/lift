const express = require('express')

const app = express()
require('../boot/setup.js')(app)
require('../src/entities/city/index.js')(app)
// require('../src/entities/homeAddress/index.js')(app)
// require('../src/entities/area/index.js')(app)
// require('../src/entities/neighborhood/index.js')(app)
require('../boot/db.js')(app)
require('../src/entities/admin/index.js')(app)
require('../src/entities/manager/index.js')(app)

module.exports = app
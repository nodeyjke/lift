const Joi = require('@hapi/joi')

const homeAddressSchema = Joi.object().keys({
    homeAddress: Joi.string().required(),
    isElite: Joi.boolean().required(),
    elevator: Joi.number().required()
})

module.exports = {
    homeAddressSchema
}
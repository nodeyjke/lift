const Joi = require('@hapi/joi')

const citySchema = Joi.object().keys({
    city: Joi.string().required()
})

module.exports = {
    citySchema
}
const Joi = require('@hapi/joi')

const neighborhoodSchema = Joi.object().keys({
    neighborhood: Joi.string().required()
})

module.exports = {
    neighborhoodSchema
}
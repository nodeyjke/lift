const Joi = require('@hapi/joi')

const areaSchema = Joi.object().keys({
    areaName: Joi.string().required(),
})

module.exports = {
    areaSchema
}
const nodemailer = require("nodemailer");
const config = require('config')

  // create reusable transporter object using the default SMTP transport
module.exports = { 
  sendEmail: async (email, password) => {
    const smptTransport = nodemailer.createTransport({
    host: config.get("mail.host"),
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: config.get("mail.user"), // generated ethereal user
      pass: config.get("mail.pass") // generated ethereal password
    }
  });
  try {
    await smptTransport.sendMail({
      from: config.get("mail.user"),
      to: email,
      subject: "Authorithation",
      text: 'Пароль для входа в личный кабинет ' + password
    })
  }catch(err) {
    console.log(err)
  }
}
}
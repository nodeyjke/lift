const Claim = require('./model.js')

module.exports = {

listClaim: async(req, res) => {
    let claims = await Claim.find({})
    res.json(claims)
  },
addClaim: async(req, res) => {
    try {
      const claim = new Claim({
        company: req.body.company,
        fieldOfActivity: req.body.fieldOfActivity,
        city: req.body.city,
        status: req.body.status,
      })
    await claim.save();
    res.status(201).json({claim})
  }catch(er){
    console.log(er)
  }
  },
deleteClaim: async(req, res) => {
  const claim = await Claim.findOneAndDelete({_id: req.param.id })
    if(!claim) return res.status(400).json("Claim not found")
    else{
      return res.status(200).json("Claim was deleted")
    }
  },
updateClaim: async(req, res) => {
  const claim = await Claim.findOneAndDelete(
    {_id: req.params.id}, 
    {...req.body}, 
    {new: true})
    if(!claim) return res.status(400).json("Claim not found")
    else{
      return res.status(200).json("Claim was updated")
    }
  },
}
const mongoose = require('mongoose')

const { Schema } = mongoose

const ClaimSchema = new Schema
(
    {
        company: String,
        fieldOfActivity: String,
        сity: String,
        status: {
            type: String,
            enum: ['Новая', 'Первичный контракт', 'Вторичный контракт',
            'Принимает решение', 'Успешно завершена', 'Отказ']
        }
    }, 
    {timestamps: true}
)

const Claim = mongoose.model('Claim', ClaimSchema)
module.exports = Claim
const express = require('express')
const ManagerController = require('../manager/controller.js')
const AdminController = require('./controller.js')
const RequestController = require('../request/controller.js')
const ClientController = require('../client/controller.js')
const auth = require("./auth.js")

const router = express.Router()

router
.route('/login')
.post(AdminController.login)

router
.route('/manager')
.post(auth, ManagerController.register)
.get(auth, ManagerController.listManager)

router
.route('/manager/:id')
.delete(auth, ManagerController.deleteManager)
.patch(auth, ManagerController.updateManager)

router
.route('/client')
.get(auth, ClientController.listCleint)
.post(auth, ClientController.register)

router
.route('/client/:id')
.delete(auth, ClientController.deleteClient)
.patch(auth, ClientController.updateClient)

router
.route('/claimm')
.get(auth, RequestController.listClaim)
.post(auth, RequestController.addClaim)

router
.route('/claim/:id')
.delete(auth, RequestController.deleteClaim)
.patch(auth, RequestController.updateClaim)

module.exports = router
const routes = require('./routes')

module.exports = function admins(app)
{
    app.use('/admin', routes)
}
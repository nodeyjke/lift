const { Admin } = require("./model.js");
const Story = require('../story/model.js')

module.exports = {
  login: async (req, res) => {
    const ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    const { email, password } = req.body
    const admin = await Admin.findOne({ email: email })

    if (!admin) {
      return res.status(400).json("Wrong email or password")
    } else {
      const isValid = await admin.comparePassword(password)
      if (!isValid) {
        return res.status(400).json("Wrong email or password")
      }
    }

    const story = new Story({
      ip: ip,
      login: admin.name
    }
    )
    story.save()
    const token = admin.generateAuthToken()
    return res.status(200).json({ token })
  }
}

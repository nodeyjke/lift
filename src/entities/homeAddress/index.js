const routes = require('./routes')

module.exports = function adresses(app)
{
    app.use('/addresses', routes)
}
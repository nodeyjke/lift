const express = require('express')

const AddressController = require('./controller')
    
const router = express.Router()

router
.route('/')
.post(AddressController.addAddress)
.get(AddressController.findAddressys)

router
.route('/:id')
.get(AddressController.findAddress)
.patch(AddressController.updateAddress)
.delete(AddressController.deleteAddress)

module.exports = router
const mongoose = require('mongoose')

const { Schema } = mongoose

const AddressSchema = new Schema
(
    {
        homeAddress: 
        {
            type: String,
            trim: true,
            required: true,
            unique: true
        },
        neighborhood: 
        {
            type: Schema.Types.ObjectId,
            ref: 'Neighborhood'
        },
        area:
        {
            type: Schema.Types.ObjectId,
            ref: 'Area'
        },
        isElite: Boolean,
        elevator: Number
    }
)

const Address = mongoose.model('Address', AddressSchema)
module.exports = Address
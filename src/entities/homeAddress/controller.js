const Address = require('./model.js')

module.exports = {
    addAddress: async(req, res, next) => 
    {
        const { homeAddress, areaId, neighborhoodId, elite, elevator} = req.body
        try{
            const address = new Address
            (
                {
                    adress: homeAddress,
                    isElite: elite,
                    elevator: elevator,
                    area: areaId,
                    neighborhood: neighborhoodId
                }
            )
            await address.save()
            res.status(201).json(address)
        }
        catch(error)
        {
            next(error)
        }
    },

    findAddress: async(req, res) => 
    {
        const{ id: addressId } = req.params
        const address = await Address.findOne({ _id: addressId})
        res.status(200).json(address)
    },

    findAddressys: async(res) => 
    {
        const addresses = await Adress.find({})

        const addressList = {}
        addresses.forEach((address) => 
        {
            addressList[address._id] = address;
        })
        res.status(200).send(addressList)
    },

    deleteAddress: async(req, res, next) => 
    {
        const{ id: addressId } = req.params;
        try
        {
            Address.deleteOne({ _id: addressId })
            res.status(204).json()
        }
        catch(error)
        {
            next(error)
        }
    },

    updateAddress: async(req, res, next) => 
    {
        const field = req.body
        const { id: addressId } = req.params
        try {
            const address = await Address.findOne({ _id: addressId })
            if(!address)
            {
                return res.status(404).json(
                    {
                    error: "Address not found"
                    }
                )
            }
            await Address.updateOne({_id: addressId}, field)
            res.status(200).json()
        }
        catch(error)
        {
            next(error)
        }
    }
}
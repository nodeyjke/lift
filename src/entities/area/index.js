const routes = require('./routes')

module.exports = function areas(app)
{
    app.use('/areas', routes)
}
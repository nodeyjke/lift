const express = require('express')

const AreaController = require('./controller')

const router = express.Router()

router
.route('/')
.post(AreaController.addArea)
.get(AreaController.findAreas)

router
.route('/:id')
.get(AreaController.findArea)
.patch(AreaController.updateArea)
.delete(AreaController.deleteArea)

module.exports = router
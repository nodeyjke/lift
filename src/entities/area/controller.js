const Area = require('./model.js')

module.exports = {
    addArea: async(req, res, next) => 
    {
        const {areaName, cityId} = req.body
        try
        { 
            const area = await new Area({area: areaName}, {city: cityId})
            await area.save()
            res.status(201).json(area)
        }
        catch(error)
        {
            next(error)
        }
    },

    findArea: async(req, res) => 
    {
        const{ id: areaId } = req.params
        const area = await Area.findOne({ _id: areaId})
        res.status(200).json(area)
    },

    findAreas: async(res) => 
    {
        const areas = await Area.find({})

        const areaList = {}
        areas.forEach((area) => 
        {
            areaList[area._id] = area;
        })
        res.status(200).send(areaList)
    },

    deleteArea: async(req, res, next) => 
    {
        const{ id: areaId } = req.params;
        try
        {
            Area.deleteOne({ _id: areaId })
            res.status(204).json()
        }
        catch(error) 
        {
            next(error)
        }
    },

    updateArea: async(req, res, next) => 
    {
        const field = req.body
        const { id: areaId } = req.params
        try
        {
            const area = await Area.findOne({ _id:  areaId })
            if(!area)
            {
                return res.status(404).json(
                    {
                    error: "area not found"
                    }
                )
            }
            await Area.updateOne({_id: areaId}, field)
            res.status(200).json()
        }
        catch(error)
        {
            next(error)
        }
    }
}
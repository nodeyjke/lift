const mongoose = require('mongoose')

const { Schema } = mongoose

const AreaSchema = new Schema
(
    {
        areaName: 
        {
            type: String,
            trim: true,
            unique: true,
            required: true
        },
        city: 
        {
            type: Schema.Types.ObjectId,
            ref: 'City'
        }
    }
)

const Area = mongoose.model('Area', AreaSchema)
module.exports = Area
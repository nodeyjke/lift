const mongoose = require('mongoose')

const { Schema } = mongoose

const StorySchema = new Schema
(
    {
        ip: String,
        login: String,
    }, 
    {timestamps: true}
)

const Story = mongoose.model('Story', StorySchema)
module.exports = Story
const {Manager} = require('./model.js')
const Story = require('../story/model.js')
const sendEmail = require('../../utils/sendEmail.js')
const bcrypt = require('bcryptjs')
const generator = require('generate-password')

module.exports = {
  register: async (req, res) => {
    const password = generator.generate(({
      length: 8,
      numbers: true,
    }))
        try{
          const manager = new Manager({
          surname: req.body.surname,
          name: req.body.name,
          patronimyc: req.body.patronimyc,
          email: req.body.email,
          phone: req.body.phone,
          password: password,
        });
        manager.password = await bcrypt.hash(manager.password, 10);
        await manager.save();
        sendEmail.sendEmail(manager.email, password)
        res.status(200).json({
          _id: manager._id,
          surname: manager.surname,
          name: manager.name,
          patronimyc: manager.patronimyc,
          email: manager.email,
          phone: manager.phone,
        });
      }
        catch(ex){
          console.log(ex)
        }
  },
  login: async(req, res) => {
  const ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
  const { email, password} = req.body
  const manager = await Manager.findOne({ email: email})
  const pass = await manager.comparePassword(password)
  if(!manager) {
    return res.status(400).json({
      text: "Manager not found"
    })
  }
  if(!pass) {
    return res.status(400).json({
      text: "Wrong password"
    })
  }
  else{
    const story = new Story({
      ip: ip,
      login: manager.email
    }
    )
    story.save()
    const token = manager.generateAuthToken()
    res.header('x-auth-token', token)
    return res.status(200).json({token})
  }
  },
  updateManager: async(req, res) => {
      const manager = await Manager.findOneAndUpdate(
      { _id: req.params.id },
      { ...req.body }, 
      { new: true }
    );
    if(!manager) return res.status(400).json("Manager not found")
    else{
      return res.status(200).json("Request was updated")
    }
  },
  deleteManager: async(req, res) => {
    if(await Manager.deleteOne({_id: req.params.id})){
    return res.status(200).json("Manager was deleted")
  }
  },
  listManager: async(req, res) => {
    let managers = await Manager.find({})
    res.json({managers})
  },
}
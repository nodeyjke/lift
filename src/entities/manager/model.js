const mongoose = require('mongoose')
const Joi = require('joi')
const config = require('config')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const { Schema } = mongoose

const ManagerSchema = new Schema({
    surname: String,
    name: String,
    patronimyc: String,
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
      },
    phone: String,
    password: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 255
      },
})

ManagerSchema.methods.generateAuthToken = function() { 
  const token = jwt.sign({ _id: this._id}, config.get('myprivatekey')); //get the private key from the config file -> environment variable
  return token;
}

ManagerSchema.methods.comparePassword = function(candidatePassword) {
 const password = bcrypt.compare(candidatePassword, this.password)
  return password;
};

function validateUser(user) {
    const schema = {
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(3).max(255).required()
    };
  
    return Joi.validate(user, schema);
  }

const Manager = mongoose.model('Manager', ManagerSchema)
exports.Manager = Manager
exports.validate = validateUser;
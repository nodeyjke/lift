const express = require('express')

const ManagerController = require('./controller.js')
const auth = require("./auth.js")

const router = express.Router()

router
.route('/login')
.post(ManagerController.login)

module.exports = router
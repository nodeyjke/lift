const mongoose = require('mongoose')

const { Schema } = mongoose

const NeighborhoodSchema = new Schema
(
    {
        area: 
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Area'
        },
        neighborhood:
        {
            type: String,
            trim: true,
            unique: true,
            required: true
        }
    }
)

const Neighborhood = mongoose.model('Neighborhood', NeighborhoodSchema)
module.exports = Neighborhood
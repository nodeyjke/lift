const express = require('express')

const NeighborhoodController = require('../neighborhood/cotroller.js')

const router = express.Router()

router
.route('/')
.post(NeighborhoodController.addNeighborhood)
.get(NeighborhoodController.findNeighborhoods)

router
.route('/:id')
.get(NeighborhoodController.findNeighborhood)
.patch(NeighborhoodController.updateNeighborhood)
.delete(NeighborhoodController.deleteNeighborhood)

module.exports = router
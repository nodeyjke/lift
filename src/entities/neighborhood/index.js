const routes = require('./routes')

module.exports = function neighborhoods(app) 
{
    app.use('/neighborhoods', routes)
}
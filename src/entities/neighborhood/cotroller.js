const Neighborhood = require('./model.js')

module.exports = {
    addNeighborhood: async(req, res, next) => 
    {
        const {neighborhoodName, areaId} = req.body
        try
        {
            const neighborhood = await new Neighborhood
            (
                {neighborhood: neighborhoodName},
                {area: areaId}
            )
            await neighborhood.save()
            res.status(201).json(neighborhood)
        }
        catch(error)
        {
            next(error)
        }
    },

    findNeighborhood: async(req, res) => 
    {
        const{ id: neighborhoodId } = req.params
        const neighborhood = await Neighborhood.findOne({ _id: neighborhoodId})
        res.status(200).json(neighborhood)
    },

    findNeighborhoods: async(req, res) => 
    {
        const neighborhoods = await Neighborhood.find({})
        const neighborhoodList = {}
        neighborhoods.forEach((neighborhood) =>
        {
            NeighborhoodList[neighborhood._id] = neighborhood;
        }
        )
        res.status(200).send(neighborhoodList)
    },

    deleteNeighborhood: async(req, res, next) =>
    {
        const{ id: neighborhoodId } = req.params

        try
        {
            const neighborhood = await Neighborhood.findOne({ _id:  neighborhoodId })
            if(!neighborhood)
            {
                return res.status(404).json(
                {
                    error: "Neighborhood not found"
                })
            }
            await Neighborhood.deleteOne({ _id: neighborhoodId })
            res.status(204).json()
        }
        catch(error)
        {
            next(error)
        }
    },

    updateNeighborhood: async(req, res, next) =>
    {
        const field = req.body
        const { id: neighborhoodId } = req.params
        try
        {
            const neighborhood = await Neighborhood.findOne({ _id:  neighborhoodId })
            if(!neighborhood)
            {
                return res.status(404).json(
                {
                    error: "Neighborhood not found"
                })
            }
            await Neighborhood.updateOne({_id: neighborhoodId}, field)
            res.status(200).json(neighborhood)
        }
        catch(error)
        {
            next(error)
        }
    }
}
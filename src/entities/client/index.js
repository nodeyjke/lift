const routes = require('./routes')

module.exports = function managers(app)
{
    app.use('/client', routes)
}
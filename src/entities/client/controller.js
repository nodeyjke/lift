const {Client} = require('./model.js')

module.exports = {
  register: async (req, res) => {
      try{
          const client = new Client(req.body);
        await client.save();
        res.status(200).json(client);
    }
        catch(ex){
          console.log(ex)
        }
  },
//   login: async(req, res) => {
//   const ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
//   const { email, password} = req.body
//   const manager = await Manager.findOne({ email: email})
//   const pass = await manager.comparePassword(password)
//   if(!manager) {
//     return res.status(400).json({
//       text: "Manager not found"
//     })
//   }
//   if(!pass) {
//     return res.status(400).json({
//       text: "Wrong password"
//     })
//   }
//   else{
//     const story = new Story({
//       ip: ip,
//       login: manager.email
//     }
//     )
//     story.save()
//     const token = manager.generateAuthToken()
//     res.header('x-auth-token', token)
//     return res.status(200).json({token})
//   }
//   },
  updateClient: async(req, res) => {
      const client = await Client.findOneAndUpdate(
      { _id: req.params.id },
      { ...req.body }, 
      { new: true }
    );
    if(!client) return res.status(400).json("Client not found")
    else{
      return res.status(200).json("Client was updated")
    }
  },
  deleteClient: async(req, res) => {
    if(await Client.deleteOne({_id: req.params.id})){
    return res.status(200).json("Client was deleted")
  }
  },
  listCleint: async(req, res) => {
    let clients = await Client.find({})
    res.json({clients})
  },
}
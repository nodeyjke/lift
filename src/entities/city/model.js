const mongoose = require('mongoose')

const { Schema } = mongoose

const CitySchema = new Schema
(
    {
        city: 
        {
            type: String,
            required: true,
            unique: true
        }
    }
)

const City = mongoose.model('City', CitySchema)
module.exports = City
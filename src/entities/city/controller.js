const City = require('./model.js')

module.exports = {
    addCity: async(req, res, next) => 
    {
        const {cityName} = req.body
        try
        {
            const city = new City({city: cityName})
            await city.save()
            res.status(201).json(city)
        }
        catch(error)
        {
            next(error)
        }
    },

    findCity: async(req, res) => {
        const{ id: cityId } = req.params
        const city = await City.findOne({ _id: cityId})
        res.status(200).json(city)
    },

    findCities: async(res) => 
    {
        const citys = await City.find({})

        const cityList = {}
        citys.forEach((city) => 
        {
            cityList[city._id] = city;
        })
        res.status(200).send(cityList)
    },

    deleteCity: async(req, res, next) => 
    {
        const{ id: cityId } = req.params;
        try
        {
            City.remove({ _id: cityId })
            res.status(204).json()
        }
        catch(error)
        {
            next(error)
        }
    },

    updateCity: async(req, res, next) => 
    {
        const field = req.body
        const { id: cityId } = req.params
        try
        {
            const city = await City.findOne({ _id:  cityId })
            if(!city)
            {
                return res.status(404).json
                (
                    {
                    error: "City not found"
                    }
                )
            }
            await City.updateOne({_id: cityId}, field)
            res.status(200).json(city)
        }
        catch(error) 
        {
            next(error)
        }
    }
}
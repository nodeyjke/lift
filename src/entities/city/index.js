const routes = require('./routes')

module.exports = function citys(app)
{
    app.use('/cities', routes)
}
const express = require('express')
const { check, validationResult } = require('express-validator')

const CityController = require('./controller')

const router = express.Router()

router
.route('/')
.post(CityController.addCity)
.get(CityController.findCities)

router
.route('/:id')
.get(CityController.findCity)
.patch(CityController.updateCity)
.delete(CityController.deleteCity)

module.exports = router